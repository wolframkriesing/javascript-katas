FROM node
WORKDIR /app
COPY . /app
ENV PATH=$PATH:./node_modules/.bin
