import assert from 'assert';

describe('', () => {
  it('Function.length is always 1, the only "required" param', () => {
    assert.strictEqual(Function.length, 1);
  }); 
});