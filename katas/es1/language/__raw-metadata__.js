import {SKILL_LEVEL} from '../../skill-levels.js';
import * as tag from '../../tags.js';
import * as date from '../../date.js';

const buildReferenceForId = id => ({bundle: 'es1/language', id});
export const es1 = {
  ARRAY_SORT_BASICS: buildReferenceForId(1),
  ARRAY_SORT_WITH_FUNCTION: buildReferenceForId(2),
  GLOBAL_PARSEINT: buildReferenceForId(3),
  GLOBAL_PARSEINT_IN_DEPTH: buildReferenceForId(4),
  TYPE_CONVERSION_TO_BOOLEAN: buildReferenceForId(5),
  TYPE_CONVERSION_TO_NUMBER: buildReferenceForId(6),
  MATH_API: buildReferenceForId(7),
  ALL_UNARY_OPERATORS: buildReferenceForId(8),
  UNARY_OPERATOR_PLUS: buildReferenceForId(9),
  UNARY_OPERATOR_PLUS_IN_DEPTH: buildReferenceForId(10),
  
  BITWISE_LEFT_SHIFT_OPERATOR: buildReferenceForId(11),
  BITWISE_RIGHT_SHIFT_OPERATOR: buildReferenceForId(12),
  ALL_BITWISE_SHIFT_OPERATOR: buildReferenceForId(13),
  FUNCTION_LENGTH: buildReferenceForId(14),
  FUNCTION_CONSTRUCTOR_NEW: buildReferenceForId(15),
  FUNCTION_CONSTRUCTOR_LENGTH: buildReferenceForId(16),
  FUNCTION_CONSTRUCTOR_PROTOTYPE: buildReferenceForId(17),
  FUNCTION_ARGUMENTS: buildReferenceForId(18),
  FUNCTION_PROTOTYPE: buildReferenceForId(19),
  
  OBJECT_FUNCTION: buildReferenceForId(20),
};

export const all = {
  name: 'ECMAScript 1',
  nameSlug: 'es1-katas',
  groups: {
    'Array API': {
      items: {
        [es1.ARRAY_SORT_BASICS.id]: {
          name: '`array.sort()` basics',
          description: 'The `sort()` function sorts an array as if each element was a string.',
          path: 'array-api/sort-basics',
          level: SKILL_LEVEL.BEGINNER,
          requiresKnowledgeFrom: [],
          publishDateUTC: new Date(Date.UTC(2015, date.OCTOBER, 22, 9, 11)),
          links: [
            {
              url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort',
              comment: 'Very detailed description of how sort() works.',
              tags: [tag.MDN, tag.DOCS]
            },
            {
              url: 'https://en.wikipedia.org/wiki/List_of_Unicode_characters#Basic_Latin',
              comment: 'All the basic latin characters (close to ASCII).',
              tags: [tag.WIKIPEDIA, tag.DOCS]
            },
            {
              url: 'https://en.wikipedia.org/wiki/Emoji#Blocks',
              comment: 'Some emoji icons and their unicode data.',
              tags: [tag.WIKIPEDIA, tag.DOCS]
            },
            {
              url: 'https://twitter.com/wolframkriesing/status/657161540525826048',
              comment: 'Announcement of this kata on twitter.',
              tags: [tag.ANNOUNCEMENT]
            }
          ]
        },
        [es1.ARRAY_SORT_WITH_FUNCTION.id]: {
          name: '`array.sort()` can take a compare function',
          description: 'Passing a callback to the `sort()` function, allows for any custom sorting.',
          path: 'array-api/sort-with-function',
          level: SKILL_LEVEL.INTERMEDIATE,
          requiresKnowledgeFrom: [
            es1.ARRAY_SORT_BASICS
          ],
          publishDateUTC: new Date(Date.UTC(2015, date.OCTOBER, 23, 9, 26)),
          links: [
            {
              url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort',
              comment: 'Very detailed description of how `sort()` works.',
              tags: [tag.MDN, tag.DOCS]
            },
            {
              url: 'https://twitter.com/wolframkriesing/status/657488332298043392',
              comment: 'Announcement of this kata on twitter.',
              tags: [tag.ANNOUNCEMENT]
            }
          ]
        }
      }
    },
    'Global Object API': {
      items: {
        [es1.GLOBAL_PARSEINT.id]: {
          name: '`parseInt()`',
          description: '`parseInt()` converts a given value to a string and then tries to make an integer out of it.',
          path: 'global-api/parseInt',
          level: SKILL_LEVEL.BEGINNER,
          requiresKnowledgeFrom: [],
          publishDateUTC: new Date(Date.UTC(2019, date.OCTOBER, 6, 19, 53)),
          links: [
            {
              url: 'https://www.ecma-international.org/publications/files/ECMA-ST-ARCH/ECMA-262,%201st%20edition,%20June%201997.pdf',
              comment: 'The very first version of the spec defining `parseInt`, the ES1 spec.',
              tags: [tag.SPECIFICATION, tag.DOCS]
            },
            {
              url: 'https://www.ecma-international.org/ecma-262/10.0/index.html#sec-parseint-string-radix',
              comment: 'A later, newer version of the spec text for `parseInt`, from ES10.',
              tags: [tag.SPECIFICATION, tag.DOCS]
            },
            {
              url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/parseInt',
              comment: 'The description of `parseInt()` on MDN, probably best to read.',
              tags: [tag.MDN, tag.DOCS]
            },
            {
              url: 'https://twitter.com/wolframkriesing/status/1180943849486716928',
              comment: 'Announcement of this kata on twitter.',
              tags: [tag.ANNOUNCEMENT]
            },
          ],
        },
        [es1.GLOBAL_PARSEINT_IN_DEPTH.id]: {
          name: '`parseInt()` in depth',
          description: '`parseInt()` converts a given value to a string and then tries to make an integer out of it.',
          path: 'global-api/parseInt-in-depth',
          level: SKILL_LEVEL.BEGINNER,
          requiresKnowledgeFrom: [],
          // publishDateUTC: new Date(Date.UTC(2019, date.OCTOBER, 6, 19, 53)),
          links: []
        },
      },
    },
    'Type conversion': {
      items: {
        [es1.TYPE_CONVERSION_TO_BOOLEAN.id]: {
          name: 'to boolean',
          description: 'How do different types convert to a boolean?',
          path: 'type-conversion/to-boolean',
          level: SKILL_LEVEL.INTERMEDIATE,
          requiresKnowledgeFrom: [
            es1.ALL_UNARY_OPERATORS,
          ],
          publishDateUTC: new Date(Date.UTC(2023, date.NOVEMBER, 7, 0, 16)),
          links: [
          ],
        },
        [es1.TYPE_CONVERSION_TO_NUMBER.id]: {
          name: 'to number',
          description: 'How do different types convert to a number?',
          path: 'type-conversion/to-number',
          level: SKILL_LEVEL.INTERMEDIATE,
          requiresKnowledgeFrom: [
            es1.ALL_UNARY_OPERATORS,
          ],
          // publishDateUTC: new Date(Date.UTC(2019, date.OCTOBER, 6, 19, 53)),
          links: [
          ],
        },
      },
    },
    'Unary Operators': {
      items: {
        [es1.UNARY_OPERATOR_PLUS.id]: {
          name: 'Unary "+" operator',
          description: 'converts its operand to the Number type',
          path: 'unary-operators/plus',
          level: SKILL_LEVEL.INTERMEDIATE,
          requiresKnowledgeFrom: [
            es1.ALL_UNARY_OPERATORS,
          ],
          publishDateUTC: new Date(Date.UTC(2021, date.FEBRUARY, 7, 1, 1)),
          links: [
            {
              url: 'https://www.ecma-international.org/wp-content/uploads/ECMA-262_1st_edition_june_1997.pdf',
              comment: 'The original ECMAScript 1 specification, "Unary + Operator" is on page 40, chapter 11.4.6 (in a 110 pages PDF).',
              tags: [tag.SPECIFICATION],
            },
            {
              url: 'https://www.ecma-international.org/wp-content/uploads/ECMA-262_1st_edition_june_1997.pdf',
              comment: 'The original ES1 spec describes the `Number` type in chapter 8.5 (a 110 pages PDF).',
              tags: [tag.SPECIFICATION],
            },
            {
              url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Unary_plus',
              comment: 'The MDN page.',
              tags: [tag.MDN],
            },
            {
              url: 'https://twitter.com/jskatas/status/1358119660600889351',
              comment: 'Tweeting that not-assertions make sense, sometimes.',
              tags: [tag.SOCIAL_MEDIA_POST],
            },
          ],
        },
        [es1.UNARY_OPERATOR_PLUS_IN_DEPTH.id]: {
          name: 'Unary "+" operator in depth',
          description: 'converts its operand to the Number type',
          path: 'unary-operators/plus-in-depth',
          level: SKILL_LEVEL.ADVANCED,
          requiresKnowledgeFrom: [
            es1.ALL_UNARY_OPERATORS,
            es1.UNARY_OPERATOR_PLUS,
          ],
          publishDateUTC: new Date(Date.UTC(2021, date.FEBRUARY, 7, 1, 31)),
          links: [
          ],
        },
        [es1.ALL_UNARY_OPERATORS.id]: {
          name: 'All unary operators',
          description: 'unary - an operation with only one operand',
          path: 'unary-operators/all',
          level: SKILL_LEVEL.BEGINNER,
          requiresKnowledgeFrom: [
          ],
          publishDateUTC: new Date(Date.UTC(2021, date.FEBRUARY, 7, 23, 50)),
          links: [
          ],
        },
      },
    },
    'Bitwise Shift Operators': {
      items: {
        [es1.BITWISE_LEFT_SHIFT_OPERATOR.id]: {
          name: 'Left Shift "<<"',
          description: 'shifts the left operand\'s value x bits to the left in binary representation',
          path: 'bitwise-shift-operators/left-shift',
          level: SKILL_LEVEL.INTERMEDIATE,
          requiresKnowledgeFrom: [
          ],
          publishDateUTC: new Date(Date.UTC(2023, date.SEPTEMBER, 1, 17, 0)),
          links: [
            {
              url: 'https://www.ecma-international.org/wp-content/uploads/ECMA-262_1st_edition_june_1997.pdf',
              comment: 'The original ECMAScript 1 specification, "The left shift operator ( << )" is on page 44, chapter 11.7.1 (in a 110 pages PDF).',
              tags: [tag.SPECIFICATION],
            },
            {
              url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Left_shift',
              comment: 'The MDN page.',
              tags: [tag.MDN],
            },
            {
              url: 'https://mastodontech.de/@wolframkriesing/110968248279414969',
              comment: 'Mastodon toot about starting to dive into the rabbit hole about writing this kata.',
              tags: [tag.SOCIAL_MEDIA_POST],
            },
          ],
        },
        [es1.BITWISE_RIGHT_SHIFT_OPERATOR.id]: {
          name: 'Right Shift ">>"',
          description: 'shifts the left operand\'s value x bits to the right in binary representation',
          path: 'bitwise-shift-operators/right-shift',
          level: SKILL_LEVEL.INTERMEDIATE,
          requiresKnowledgeFrom: [
            es1.TYPE_CONVERSION_TO_NUMBER,
          ],
          publishDateUTC: new Date(Date.UTC(2023, date.SEPTEMBER, 4, 17, 50)),
          links: [
            {
              url: 'https://www.ecma-international.org/wp-content/uploads/ECMA-262_1st_edition_june_1997.pdf',
              comment: 'The original ECMAScript 1 specification, "The right shift operator ( << )" is on page 44, chapter 11.7.2 (in a 110 pages PDF).',
              tags: [tag.SPECIFICATION],
            },
            {
              url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Right_shift',
              comment: 'The MDN page.',
              tags: [tag.MDN],
            },
            {
              url: 'https://mastodontech.de/@wolframkriesing/110968248279414969',
              comment: 'Mastodon toot about starting to dive into the rabbit hole about writing this kata.',
              tags: [tag.SOCIAL_MEDIA_POST],
            },
          ],
        },
        [es1.ALL_BITWISE_SHIFT_OPERATOR.id]: {
          name: 'All bitwise shift operators',
          description: 'bitwise shift - shifts the given number of bits modifying them on the binary level',
          path: 'bitwise-shift-operators/all',
          level: SKILL_LEVEL.BEGINNER,
          requiresKnowledgeFrom: [
          ],
          publishDateUTC: new Date(Date.UTC(2023, date.SEPTEMBER, 4, 17, 50)),
          links: [
            {
              url: 'https://www.ecma-international.org/wp-content/uploads/ECMA-262_1st_edition_june_1997.pdf',
              comment: 'The original ECMAScript 1 specification, "The unsigned right shift operator ( >>> )" is on page 45, chapter 11.7.3 (in a 110 pages PDF).',
              tags: [tag.SPECIFICATION],
            },
            {
              url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Unsigned_right_shift',
              comment: 'The MDN page.',
              tags: [tag.MDN],
            },
          ],
        },
      },
    },
    'function API': {
      items: {
        [es1.FUNCTION_LENGTH.id]: {
          name: '`function.length` (as introduced in ES1)',
          description: 'The value of the `length` property (an integer) indicates the "typical" number of arguments expected by the function',
          path: 'function-api/length',
          level: SKILL_LEVEL.BEGINNER,
          requiresKnowledgeFrom: [],
          publishDateUTC: new Date(Date.UTC(2023, date.OCTOBER, 14, 12, 15)),
          links: [
            {
              url: 'https://www.ecma-international.org/wp-content/uploads/ECMA-262_1st_edition_june_1997.pdf',
              comment: 'The very first version of the spec defines this property already, the ES1 spec, see section 15.3.5.1 (PDF 732kB).',
              tags: [tag.SPECIFICATION, tag.DOCS]
            },
            {
              url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/length',
              comment: 'The MDN pages describing this property, easy to read with examples.',
              tags: [tag.MDN, tag.DOCS]
            },
            {
              url: 'https://mastodontech.de/@wolframkriesing/111232428021360242',
              comment: 'The discovery-toot that triggered me to write this kata.',
              tags: [tag.SOCIAL_MEDIA_POST]
            },
          ],
        },
        [es1.FUNCTION_ARGUMENTS.id]: {
          name: '`function.arguments` (as introduced in ES1)',
          description: 'The `arguments` property of a function is an object that contains all the arguments passed to the function.',
          path: 'function-api/arguments',
          level: SKILL_LEVEL.BEGINNER,
          requiresKnowledgeFrom: [],
          // publishDateUTC: new Date(Date.UTC(2023, date.OCTOBER, 14, 12, 15)),
          links: [
            {
              url: 'https://www.ecma-international.org/wp-content/uploads/ECMA-262_1st_edition_june_1997.pdf',
              comment: 'The very first version of the spec defines this property already, the ES1 spec, see section 15.3.5.3 (PDF 732kB).',
              tags: [tag.SPECIFICATION, tag.DOCS],
            },
          ],
        },
        [es1.FUNCTION_PROTOTYPE.id]: {
          name: '`function.prototype`',
          description: 'The `prototype` property of a function is an object that is used to implement inheritance and shared properties.',
          path: 'function-api/prototype',
          level: SKILL_LEVEL.BEGINNER,
          requiresKnowledgeFrom: [],
          // publishDateUTC: new Date(Date.UTC(2023, date.OCTOBER, 14, 12, 15)),
          links: [
            {
              url: 'https://www.ecma-international.org/wp-content/uploads/ECMA-262_1st_edition_june_1997.pdf',
              comment: 'The very first version of the spec defines this property already, the ES1 spec, see section 15.3.5.2 (PDF 732kB).',
              tags: [tag.SPECIFICATION, tag.DOCS],
            },
          ],
        },
      },
    },
    'Function constructor': {
      items: {
        [es1.FUNCTION_CONSTRUCTOR_NEW.id]: {
          name: '`new Function()` and `Function()`',
          description: 'With the `Function` constructor one can dynamically create a `Function` object.',
          path: 'Function-constructor/new',
          level: SKILL_LEVEL.INTERMEDIATE,
          requiresKnowledgeFrom: [],
          // publishDateUTC: new Date(Date.UTC(2023, date.OCTOBER, 14, 12, 15)),
          links: [
            {
              url: 'https://www.ecma-international.org/wp-content/uploads/ECMA-262_1st_edition_june_1997.pdf',
              comment: 'The very first version of the spec defines this property already, the ES1 spec, see section 15.3.2.1 (PDF 732kB).',
              tags: [tag.SPECIFICATION, tag.DOCS],
            },
            {
              url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/Function',
              comment: 'The MDN pages describing this property, easy to read with examples.',
              tags: [tag.MDN, tag.DOCS],
            },
          ],
        },
        [es1.FUNCTION_CONSTRUCTOR_LENGTH.id]: {
          name: '`Function.length`',
          description: 'The `length` property of the `Function` constructor is always 1.',
          path: 'Function-constructor/length',
          level: SKILL_LEVEL.BEGINNER,
          requiresKnowledgeFrom: [
            es1.FUNCTION_LENGTH,
          ],
          // publishDateUTC: new Date(Date.UTC(2023, date.OCTOBER, 14, 12, 15)),
          links: [
            {
              url: 'https://www.ecma-international.org/wp-content/uploads/ECMA-262_1st_edition_june_1997.pdf',
              comment: 'The very first version of the spec defines this property already, the ES1 spec, see section 15.3.3.2 (PDF 732kB).',
              tags: [tag.SPECIFICATION, tag.DOCS],
            },
          ],
        },
        [es1.FUNCTION_CONSTRUCTOR_PROTOTYPE.id]: {
          name: '`Function.prototype`',
          description: 'The `prototype` property of the `Function` constructor is a `Function` object.',
          path: 'Function-constructor/prototype',
          level: SKILL_LEVEL.BEGINNER,
          requiresKnowledgeFrom: [
          ],
          // publishDateUTC: new Date(Date.UTC(2023, date.OCTOBER, 14, 12, 15)),
          links: [
            {
              url: 'https://www.ecma-international.org/wp-content/uploads/ECMA-262_1st_edition_june_1997.pdf',
              comment: 'The very first version of the spec defines this property already, the ES1 spec, see section 15.3.4 (PDF 732kB).',
              tags: [tag.SPECIFICATION, tag.DOCS],
            },
          ],
        },
      }
    },
    'Object API': {
      items: {
        [es1.OBJECT_FUNCTION.id]: {
          name: '`Object()` (as introduced in ES1)',
          description: 'The `Object()` constructor called as a function performs a type conversion.',
          path: 'object-api/constructor',
          level: SKILL_LEVEL.INTERMEDIATE,
          requiresKnowledgeFrom: [],
          publishDateUTC: new Date(Date.UTC(2024, date.MARCH, 27, 22, 42)),
          links: [
            {
              url: 'https://www.ecma-international.org/wp-content/uploads/ECMA-262_1st_edition_june_1997.pdf',
              comment: 'The first version of the spec already has the `ToObject` (see section 9.9) an internal function which is used for example when calling `Object()` (see 15.2.1.1). (PDF 732kB).',
              tags: [tag.SPECIFICATION, tag.DOCS],
            },
            {
              url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/Object',
              comment: 'The MDN pages describing this function, easy to read with examples.',
              tags: [tag.MDN, tag.DOCS],
            },
          ],
        },
      },
    },
  },
};
