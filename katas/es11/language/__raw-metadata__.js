import {SKILL_LEVEL} from '../../skill-levels.js';
import * as tag from '../../tags.js';
import {toUtcDate} from '../../date.js';
import * as date from '../../date.js';
import {es6} from '../../es6/language/__raw-metadata__.js';
import {es5} from "../../es5/language/__raw-metadata__.js";

const buildReferenceForId = id => ({bundle: 'es11/language', id});
export const es11 = {
  STRING_MATCHALL: buildReferenceForId(1),
  BIGINT_BASICS: buildReferenceForId(2),
  STRING_MATCHALL_IN_DEPTH: buildReferenceForId(3),
};

export const all = {
  name: 'ECMAScript 11 / ES2020',
  nameSlug: 'es11-katas',
  groups: {
    'String API': {
      items: {
        [es11.STRING_MATCHALL.id]: {
          name: '`string.matchAll()`',
          description: 'Returns all results matching a string or regular expression.',
          path: 'string-api/matchAll',
          level: SKILL_LEVEL.ADVANCED,
          requiresKnowledgeFrom: [
            es6.CONST,
            es6.ARRAY_FROM,
            es6.ITERATOR_ARRAY,
            es6.SPREAD_WITH_ARRAYS,
            // FOR_OF
          ],
          publishDateUTC: toUtcDate(2022, date.MARCH, 12, 21, 53),
          links: [
            {
              url: 'https://github.com/tc39/proposal-string-matchall',
              comment: 'The repository where the proposal was worked on, some interesting details in there about naming and the process of how this became a standard.',
              tags: [tag.SPECIFICATION, tag.DISCUSSION],
            },
            {
              url: 'https://tc39.es/ecma262/#sec-string.prototype.matchall',
              comment: 'The "ECMAScript Language Specification", the JavaScript specification text describing this function.',
              tags: [tag.SPECIFICATION],
            },
            {
              url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/matchAll',
              comment: 'The Mozilla Developer Network docs, contains good examples.',
              tags: [tag.MDN, tag.DOCS],
            },
            {
              url: 'https://stackoverflow.com/questions/61173919/why-does-the-new-matchall-in-javascript-return-an-iterator-vs-an-array',
              comment: '"Why does the new "matchAll" in Javascript return an iterator (vs. an array)?"',
              tags: [tag.ARTICLE, tag.DISCUSSION],
            },
            {
              url: 'https://twitter.com/jskatas/status/1502755108911730694',
              comment: 'Announcement of this kata on twitter.',
              tags: [tag.ANNOUNCEMENT, tag.SOCIAL_MEDIA_POST]
            },
          ],
        },
        [es11.STRING_MATCHALL_IN_DEPTH.id]: {
          name: '`string.matchAll()` in depth',
          description: 'Returns all results matching a string or regular expression.',
          path: 'string-api/matchAll-in-depth',
          level: SKILL_LEVEL.ADVANCED,
          requiresKnowledgeFrom: [
            es11.STRING_MATCHALL,
            es5.FUNCTION_BIND,
          ],
          // publishDateUTC: toUtcDate(2022, date.MARCH, 12, 21, 53),
          links: [
          ],
        },
      },
    },
    'BigInt': {
      items: {
        [es11.BIGINT_BASICS.id]: {
          name: 'basics',
          description: 'A `BigInt` can represent numbers larger than `number`',
          path: 'bigint/basics',
          level: SKILL_LEVEL.BEGINNER,
          requiresKnowledgeFrom: [],
          publishDateUTC: toUtcDate(2022, date.MARCH, 10, 23, 59),
          links: [
            {
              url: 'https://github.com/tc39/proposal-bigint',
              comment: 'The proposal repo.',
              tags: [tag.SPECIFICATION, tag.DISCUSSION],
            },
            {
              url: 'https://tc39.es/ecma262/#sec-bigint-constructor',
              comment: 'The "ECMAScript Language Specification", the JavaScript specification text describing this function.',
              tags: [tag.SPECIFICATION],
            },
            {
              url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/BigInt',
              comment: 'The Mozilla Developer Network docs, contains good examples.',
              tags: [tag.MDN, tag.DOCS],
            },
            {
              url: 'https://twitter.com/jskatas/status/1502068572788604929',
              comment: 'Announcement of this kata on twitter.',
              tags: [tag.ANNOUNCEMENT, tag.SOCIAL_MEDIA_POST]
            },
          ],
        },
      },
    },
  },
};
