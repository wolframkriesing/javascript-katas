import {SKILL_LEVEL} from '../../skill-levels.js';
import * as tag from '../../tags.js';
import {toUtcDate} from '../../date.js';
import * as date from '../../date.js';

const buildReferenceForId = id => ({bundle: 'es14/language', id});
export const es14 = {
  ARRAY_TO_REVERSED: buildReferenceForId(1),
};

export const all = {
  name: 'ECMAScript 14 / ES2023',
  nameSlug: 'es14-katas',
  groups: {
    'Array API': {
      items: {
        [es14.ARRAY_TO_REVERSED.id]: {
          name: '`array.toReversed()`',
          description: 'Returns a new array with the elements reversed.',
          path: 'array-api/toReversed',
          level: SKILL_LEVEL.BEGINNER,
          requiresKnowledgeFrom: [
          ],
          publishDateUTC: toUtcDate(2024, date.JANUARY, 16, 0, 50),
          links: [
            {
              url: 'https://github.com/tc39/proposal-change-array-by-copy',
              comment: 'The repository where the proposal was worked on. Make sure to read the polyfill.js source code in there, shows very well how `toReversed()` works.',
              tags: [tag.SPECIFICATION, tag.DISCUSSION],
            },
            {
              url: 'https://262.ecma-international.org/14.0/#sec-array.prototype.toreversed',
              comment: 'The "ECMAScript Language Specification", the JavaScript specification text describing this function how an engine must implement it.',
              tags: [tag.SPECIFICATION],
            },
            {
              url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/toReversed',
              comment: 'The Mozilla Developer Network docs.',
              tags: [tag.MDN, tag.DOCS],
            },
          ],
        },
      },
    },
  },
};
