import {SKILL_LEVEL} from '../../skill-levels.js';
import * as tag from '../../tags.js';
import {toUtcDate} from '../../date.js';
import * as date from '../../date.js';
import {es14} from "../../es14/language/__raw-metadata__.js";

const buildReferenceForId = id => ({bundle: 'es15/language', id});
export const es15 = {
  OBJECT_GROUPBY: buildReferenceForId(1),
};

export const all = {
  name: 'ECMAScript 15 / ES2023',
  nameSlug: 'es15-katas',
  groups: {
    'Object API': {
      items: {
        [es15.OBJECT_GROUPBY.id]: {
          name: '`Object.groupBy()`',
          description: '',
          path: 'object-api/groupBy',
          level: SKILL_LEVEL.BEGINNER,
          requiresKnowledgeFrom: [
            es14.ARRAY_TO_REVERSED,
            // OBJECT_KEYS
          ],
          publishDateUTC: toUtcDate(2024, date.OCTOBER, 24, 13, 50),
          links: [
            {
              url: 'https://github.com/tc39/proposal-array-grouping',
              comment: 'The repository where the proposal was worked on.',
              tags: [tag.SPECIFICATION, tag.DISCUSSION],
            },
            {
              url: 'https://tc39.es/ecma262/#sec-object.groupby',
              comment: 'The "ECMAScript Language Specification", the JavaScript specification text describing this function how an engine must implement it.',
              tags: [tag.SPECIFICATION],
            },
            {
              url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/groupBy',
              comment: 'The Mozilla Developer Network docs.',
              tags: [tag.MDN, tag.DOCS],
            },
          ],
        },
      },
    },
  },
};
