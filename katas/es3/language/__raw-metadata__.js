import {SKILL_LEVEL} from '../../skill-levels.js';
import * as tag from '../../tags.js';
import * as date from '../../date.js';

const buildReferenceForId = id => ({bundle: 'es3/language', id});
export const es3 = {
  ARRAY_SHIFT: buildReferenceForId(1),
  ARRAY_PUSH: buildReferenceForId(2),
};

export const all = {
  name: 'ECMAScript 3',
  nameSlug: 'es3-katas',
  groups: {
    'Array API': {
      items: {
        [es3.ARRAY_SHIFT.id]: {
          name: '`array.shift()` (as introduced in ES3)',
          description: 'The `shift()` function removes the first element of an array and returns it.',
          path: 'array-api/shift',
          level: SKILL_LEVEL.BEGINNER,
          requiresKnowledgeFrom: [],
          publishDateUTC: new Date(Date.UTC(2023, date.OCTOBER, 11, 9, 0)),
          links: [
            {
              url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/shift',
              comment: 'Very well readable, easy to understand description of how shift() works.',
              tags: [tag.MDN, tag.DOCS]
            },
            {
              url: 'https://www.ecma-international.org/wp-content/uploads/ECMA-262_3rd_edition_december_1999.pdf',
              comment: 'The version of the specification where `array.shift()` was introduced (PDF 723kB).',
              tags: [tag.SPECIFICATION]
            },
          ]
        },
        [es3.ARRAY_PUSH.id]: {
          name: '`array.push()` (as introduced in ES3)',
          description: 'The `push()` function adds one or more elements to the end of an array.',
          path: 'array-api/push',
          level: SKILL_LEVEL.INTERMEDIATE,
          requiresKnowledgeFrom: [
          ],
          publishDateUTC: new Date(Date.UTC(2024, date.MARCH, 28, 16, 0)),
          links: [
            {
              url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/push',
              comment: 'Very well readable, easy to understand description of how push() works.',
              tags: [tag.MDN, tag.DOCS]
            },
            {
              url: 'https://www.ecma-international.org/wp-content/uploads/ECMA-262_3rd_edition_december_1999.pdf',
              comment: 'The version of the specification where `array.push()` was introduced, see section 15.4.4.7. (PDF 723kB)',
              tags: [tag.SPECIFICATION]
            },
          ]
        },
      }
    },
  },
};
