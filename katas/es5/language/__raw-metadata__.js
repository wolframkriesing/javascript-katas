import {SKILL_LEVEL} from '../../skill-levels.js';
import * as tag from '../../tags.js';
import * as date from '../../date.js';
import {es1} from "../../es1/language/__raw-metadata__.js";

const buildReferenceForId = id => ({bundle: 'es5/language', id});
export const es5 = {
  FUNCTION_BIND: buildReferenceForId(1),
  FUNCTION_LENGTH: buildReferenceForId(2),
  FUNCTION_ARGUMENTS: buildReferenceForId(3),
  STRICT_MODE_ON_OFF: buildReferenceForId(4),
};

export const all = {
  name: 'ECMAScript 5',
  nameSlug: 'es5-katas',
  groups: {
    'function API': {
      items: {
        [es5.FUNCTION_BIND.id]: {
          name: '`function.bind()`',
          description: 'The `bind()` method creates a new function with a given scope and optionally parameter(s).',
          path: 'function-api/bind',
          level: SKILL_LEVEL.ADVANCED,
          requiresKnowledgeFrom: [
            
          ],
          publishDateUTC: new Date(Date.UTC(2023, date.OCTOBER, 16, 19, 0)),
          links: [
            {
              url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind',
              comment: 'The MDN docs about `bind()`.',
              tags: [tag.MDN, tag.DOCS]
            },
            {
              url: 'https://262.ecma-international.org/5.1/#sec-15.3.4.5',
              comment: 'The ECMAScript Language Specification, 5 Edition, Section 15.3.4.5, when `bind()` was introduced.', 
              tags: [tag.SPECIFICATION]
            },
          ],
        },
        [es5.FUNCTION_LENGTH.id]: {
          name: '`function.length` (with ES5 features)',
          description: 'The `length` property indicates the number of parameters a function expects.',
          path: 'function-api/length',
          level: SKILL_LEVEL.BEGINNER,
          requiresKnowledgeFrom: [
            es5.FUNCTION_BIND,
          ],
          publishDateUTC: new Date(Date.UTC(2023, date.OCTOBER, 18, 19, 0)),
          links: [
            {
              url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/length',
              comment: 'The MDN docs about `length`.',
              tags: [tag.MDN, tag.DOCS]
            },
            {
              url: 'https://262.ecma-international.org/5.1/#sec-15.3.5.1',
              comment: 'The ECMAScript Language Specification, 5 Edition, Section 15.3.5.1.',
              tags: [tag.SPECIFICATION]
            },
          ],
        },
        [es5.FUNCTION_ARGUMENTS.id]: {
          name: '`function.arguments` (as modified since ES5)',
          description: 'The `arguments` property of a function is an object that contains all the arguments passed to the function.',
          path: 'function-api/arguments',
          level: SKILL_LEVEL.BEGINNER,
          requiresKnowledgeFrom: [],
          // publishDateUTC: new Date(Date.UTC(2023, date.OCTOBER, 14, 12, 15)),
          links: [
          ],
        },
        
      },
    },
    'Strict mode': {
      items: {
        [es5.STRICT_MODE_ON_OFF.id]: {
          name: 'Turn strict mode on/off',
          description: 'The `use strict` directive indicates that the code should be executed in "strict mode", where you can for example not use undeclared variables.',
          path: 'strict-mode/on-off',
          level: SKILL_LEVEL.ADVANCED,
          requiresKnowledgeFrom: [
            es1.FUNCTION_CONSTRUCTOR_NEW,
            // eval
            // var
          ],
          publishDateUTC: new Date(Date.UTC(2023, date.OCTOBER, 16, 19, 0)),
          links: [
            {
              url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode',
              comment: 'The MDN docs about strict mode.',
              tags: [tag.MDN, tag.DOCS],
            },
            {
              url: 'https://262.ecma-international.org/5.1/#sec-4.2.2',
              comment: 'Chapter 4.2.2 in the specification explains that and why ECMAScript offers a "strict mode" to restrict language features, and that it can coexist with non-strict code in the same program.', 
              tags: [tag.SPECIFICATION],
            },
            {
              url: 'https://262.ecma-international.org/5.1/#sec-10.1.1',
              comment: 'Chapter 10.1.1 in the specification explains how to turn strict mode on and off.',
              tags: [tag.SPECIFICATION],
            },
            {
              url: 'https://262.ecma-international.org/5.1/#sec-C',
              comment: 'Appendix C "The strict mode restriction and exceptions" explains the differences between strict mode and non-strict mode in detail.',
              tags: [tag.SPECIFICATION],
            },
          ],
        },
      },
    },
  },
};
