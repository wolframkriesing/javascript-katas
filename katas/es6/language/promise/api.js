// 78: Promise - API overview
// To do: make all tests pass, leave the assert lines unchanged!
// Follow the hints of the failure messages!

import assert from 'assert';

describe('`Promise` API overview', function() {
  it('`new Promise()` requires a function as param', () => {
    //// const param = null;
    const param = () => {};
    assert.doesNotThrow(() => { new Promise(param); });
  });
  describe('resolving a promise', () => {
    // reminder: the test passes when a fulfilled promise is returned
    it('via constructor parameter `new Promise((resolve) => { resolve(); })`', () => {
      //// const param = () => { resolve(); };
      const param = (resolve) => { resolve(); };
      return new Promise(param);
    });
    it('using `Promise.resolve()`', () => {
      //// return Promise.reject('all fine');
      return Promise.resolve('all fine');
    });
  });
  describe('a rejected promise', () => {
    it('using the constructor parameter', async () => {
      //: {"jskatas": {"runnerOptions": {"topLevelAwait": true}}}
      //// const promise = new Promise((reject) => { reject(); });
      const promise = new Promise((_, reject) => { reject(); });
      await assert.rejects(promise);
    });
    it('via `Promise.reject()`', async () => {
      //: {"jskatas": {"runnerOptions": {"topLevelAwait": true}}}
      //// const promise = Promise.resolve();
      const promise = Promise.reject();
      await assert.rejects(promise);
    });
  });
  describe('`Promise.all()`', () => {
    it('`Promise.all([p1, p2])` resolves when all promises resolve', () => {
      //// return Promise.all([Promise.resolve(), Promise.reject(), Promise.resolve()])
      return Promise.all([Promise.resolve(), Promise.resolve()])
    });
    it('`Promise.all([p1, p2])` rejects when a promise is rejected', () => {
      //// const promise = Promise.all([Promise.resolve()])
      const promise = Promise.all([Promise.resolve(), Promise.reject()])
      assert.rejects(promise);
    });
  });
  describe('`Promise.race()`', () => {
    it('`Promise.race([p1, p2])` resolves/reject when one of the promises resolves/rejects', async () => {
      //: {"jskatas": {"runnerOptions": {"topLevelAwait": true}}}
      //// const promise = Promise.race([Promise.reject(), Promise.reject()])
      const promise = Promise.race([Promise.resolve(), Promise.reject()])
      await assert.doesNotReject(promise);
    });
    it('`Promise.race([p1, p2])` rejects when one of the promises rejects', async () => {
      //: {"jskatas": {"runnerOptions": {"topLevelAwait": true}}}
      //// Promise.race([Promise.resolve()])
      const promise = Promise.race([Promise.reject(), Promise.resolve()]);
      await assert.rejects(promise);
    });
    it('`Promise.race([p1, p2])` order matters (and timing)', () => {
      //// return Promise.race([Promise.reject(), Promise.resolve()])
      return Promise.race([Promise.resolve(), Promise.reject()])
    });
  });
});
