describe('strict mode, changes since es6', () => {
  describe('strict mode is auto-turned on', () => {
    it('WHEN using modules THEN strict mode is automatically turned on', () => {
      
    });
    it('WHEN using classes THEN strict mode is automatically turned on', () => {
      
    });
    it('WHEN using arrow functions THEN strict mode is automatically turned on', () => {
      
    });
    it('WHEN using generators THEN strict mode is automatically turned on', () => {
      
    });
    it('WHEN using async functions THEN strict mode is automatically turned on', () => {
      
    });
  }); 
});