
// new primitive types
// - Symbol

// new not-primiitve types
// - Map
// - Set
// - WeakMap
// - WeakSet
// - Promise
// - Typed Arrays (like Int8Array, Uint8Array, etc.)
// - ArrayBuffer
// - Proxy
// - Intl objects (like Intl.Collator, Intl.DateTimeFormat, etc.)
// - Generator
// - GeneratorFunction
// - Reflect
// - SharedArrayBuffer
// - Atomics
// - globalThis

// - Error and its derived objects (like TypeError, ReferenceError, etc.)
// - JSON (although JSON is a namespace with static methods, not a data type)
