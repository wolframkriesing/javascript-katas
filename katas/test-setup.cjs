class FakeDomNode {
  constructor() {
    this.classList = new Set();
  }
}

const document = {
  createElement: () => {
    return new FakeDomNode();
  },
  domain: 'tddbin.com',
};

module.exports = {
  document, FakeDomNode,
};
