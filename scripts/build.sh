#!/bin/bash

# TODO: must be run from project root (i am sure this is not bash best pratice)

ORIGIN_ROOT="."
DIST_ROOT="$ORIGIN_ROOT/dist"

# clean up
rm -Rf ${DIST_ROOT}/*;

# create build directory (structure)
mkdir -p $DIST_ROOT;

# copy assets
node ./src/deploy.js
cp $ORIGIN_ROOT/CNAME $DIST_ROOT/CNAME;
# don't behave like jekyll, e.g. allow `__raw-metadata__.js` files, which would not be served otherwise
cp $ORIGIN_ROOT/.nojekyll $DIST_ROOT/;
