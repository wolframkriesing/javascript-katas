import path from 'path';

import { fileURLToPath } from 'url';
import { dirname } from 'path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

export const PROJECT_DIR = path.join(__dirname, '..');
export const KATAS_DIR = path.join(PROJECT_DIR, 'katas');
export const DIST_DIR = path.join(__dirname, '../dist');
