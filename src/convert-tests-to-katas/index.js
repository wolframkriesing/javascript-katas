import {
  allFilesInDir, mkdirp,
  readFileAsString, writeFileFromString,
} from '../_external-deps/filesystem.js';
import {FilterKatasDir} from './FilterKatasDir.js';
import {katafyFile} from './katafier.js';
import {toSrcDestPairs, createDestinationDirs} from './deploy-util.js';
import {removeImports} from "./remove-imports.js";

export const convertTestsToKatas = ({sourceDir, destinationDir}) => {
  const deps = {
    findFilenames: () => allFilesInDir(sourceDir),
    rootDir: sourceDir,
  };
  const katafyDeps = {
    readFile: readFileAsString,
    writeFile: writeFileFromString,
  };
  const toSrcDestPairsDeps = {
    sourcePath: sourceDir,
    destinationPath: destinationDir,
  };
  return FilterKatasDir(deps).forKataFiles()
    .then(files => toSrcDestPairs(files, toSrcDestPairsDeps))
    .then(pairs => createDestinationDirs(pairs, {mkdirp}).then(() => pairs))
    .then(pairs => katafy(pairs, katafyDeps))
  ;
};

const katafy = (
  katafyableList/*: KatafyableListType*/,
  {readFile, writeFile}/*: KatafyDeps*/
)/*: Promise<*>*/ => {
  const katafyableToTask = (katafyable) =>
    readFile(katafyable.sourceFilename)
      .then(katafyFile)
      .then(removeImports)
      .then(content => writeFile(katafyable.destinationFilename, content))
    ;

  return Promise.all(katafyableList.map(katafyableToTask));
};
