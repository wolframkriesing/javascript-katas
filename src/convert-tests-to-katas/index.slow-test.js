import {describe, it} from 'mocha';

describe('Convert tests to katas', () => {
  it('FIRST filter the `sourceDir` for kata files', () => {

  });
  it('AND converts the list of kata files to destination-pairs', () => {

  });
  it('AND create all missing destination directories', () => {

  });
  it('AND katafies all tests', () => {

  });
  describe('AND remove all `imports` from the kata files', () => {
    it('WHEN there is one `import assert` in the test THEN the kata file does NOT have it anymore', () => {
    });
  });
});