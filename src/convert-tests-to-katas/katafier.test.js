import {describe, it} from 'mocha';
import {assertThat, equalTo} from 'hamjest';
import {katafyFile} from './katafier.js';

describe('Katafy file content', () => {
  it('WHEN empty THEN return empty', () => {
    assertThat(katafyFile(''), equalTo(''));
  });
  it('WHEN one code line THEN leave it', () => {
    const nonKataCode = 'const some = {};';
    assertThat(katafyFile(nonKataCode), equalTo(nonKataCode));
  });
  describe('WHEN one kata line', () => {
    it('THEN remove the following and leave the kata line', () => {
      const kataCode = '////const some = {};\ncont toBeRemoved = [];';
      assertThat(katafyFile(kataCode), equalTo('const some = {};'));
    });
    it('AND leave leading spaces', () => {
      const kataCode = '  ////const some = {};\ncont toBeRemoved = [];';
      assertThat(katafyFile(kataCode), equalTo('  const some = {};'));
    });
    it('AND remove leading spaces after kata-identifier', () => {
      const kataCode = '////  const some = {};\ncont toBeRemoved = [];';
      assertThat(katafyFile(kataCode), equalTo('const some = {};'));
    });
  });
  describe('WHEN multiple kata lines, replace all following lines with kata code', () => {
    it('two sequential kata blocks', () => {
      const kataCode = [
        '////kata code',
        'to be removed',
        '////kata code 2',
        'to be removed 2'
      ].join('\n');
      assertThat(katafyFile(kataCode), equalTo('kata code\nkata code 2'));
    });
    it('with other lines inbetween', () => {
      const kataCode = [
        '',
        '////kata code',
        'to be removed',
        '// just a comment',
        '////kata code 2',
        'to be removed 2',
        ''
      ].join('\n');
      const expected = [
        '',
        'kata code',
        '// just a comment',
        'kata code 2',
        ''
      ].join('\n');
      assertThat(katafyFile(kataCode), equalTo(expected));
    });
  });
});
