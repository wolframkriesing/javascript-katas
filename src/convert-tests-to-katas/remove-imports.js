// We remove the imports, so we can just "copy+paste" the source into tddbin, where
// they are just not expected, since ever, its just history.
export const removeImports = (sourceCode) => {
  const lines = sourceCode.split('\n');
  const removedLines = []; // The indexes of the lines we removed.

  const removeImportAssert = (line, index) => {
    const trimmedLine = line.trimEnd();
    const isImportAssertLine = trimmedLine === "import assert from 'assert';" || trimmedLine === "import assert from \"assert\";";
    if (isImportAssertLine) {
      removedLines.push(index);
      return '';
    }
    return line;
  };
  const removeImportDocument = (line, index) => {
    const isImportDocumentLine = line.trimEnd().match(/^import \{document} from '(\.\.\/)+test-setup.js';/);
    if (isImportDocumentLine) {
      removedLines.push(index);
      return '';
    } else {
      return line;
    }
  };

  const isLineToBeKept = (line, index) => { 
    if (removedLines.includes(index)) {
      return false;
    }
    const isThisTheLineAfterARemovedLine = removedLines.includes(index - 1);
    const isLineEmpty = line.trim() === '';
    if (isThisTheLineAfterARemovedLine && isLineEmpty) {
      return false;
    }
    
    return true;
  };
  return lines
    .map(removeImportAssert)
    .map(removeImportDocument)
    .filter(isLineToBeKept)
    .join('\n');
};
