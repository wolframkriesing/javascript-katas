import {describe, it} from 'mocha';
import {assertThat} from "hamjest";
import {removeImports} from "./remove-imports.js";

describe('Remove import statements from kata source code', () => {
  it('WHEN the source code is empty THEN return it as is', () => {
    assertThat(removeImports(''), '');
  });
  it('WHEN the source code has no import statements THEN return it as is', () => {
    const sourceCode = `// 9: object-literals - basics
    // To do: make all tests pass, leave the assert lines unchanged!
    // Follow the hints of the failure messages!
    
    describe('The object literal allows for new shorthands', () => {
    ////
    `;
    assertThat(removeImports(sourceCode), sourceCode);
  });
  describe('WHEN the source code has `import assert`', () => {
    it('AND the module name is in single quotes THEN remove it (also ignore trailing space)', () => {
      const sourceCode = `import assert from 'assert';   `;
      assertThat(removeImports(sourceCode), '');
    });
    it('AND the module name is in double quotes THEN remove it (also ignore trailing space)', () => {
      const sourceCode = `import assert from "assert";   `;
      assertThat(removeImports(sourceCode), '');
    });
  });
  it('WHEN the import line is followed by an empty line THEN remove it too', () => {
    const sourceCode = `import assert from 'assert';   \n\n`;
    assertThat(removeImports(sourceCode), '');
  });
  describe('remove import only when it is alone on a line AND is at the beginning of the line', () => {
    it('WHEN import is not at col=0 THEN leave it', () => {
      const sourceCode = ` import assert from 'assert';`;
      assertThat(removeImports(sourceCode), sourceCode);
    });
    it('WHEN import is not "alone" on the line THEN leave it', () => {
      const sourceCode = `import assert from 'assert'; // this will make it stay`;
      assertThat(removeImports(sourceCode), sourceCode);
    });
  });

  it('WHEN source has `import {document}` THEN remove it', () => {
    const sourceCode = `import {document} from '../../../test-setup.js';   `;
    assertThat(removeImports(sourceCode), '');
  });
  it('WHEN source has `import {document}` import path is any number of "../" THEN remove it', () => {
    const sourceCode = `import {document} from '../test-setup.js';   `;
    assertThat(removeImports(sourceCode), '');
  });

  describe('remove the lines of the import', () => {
    it('WHEN a removable import is found THEN remove the entire line', () => {
      const sourceCode = `
// some comment at the beginning of the file

import assert from 'assert';      
import {document} from '../test-setup.js';   

describe('the imports below shall stay, they are not at the beginning of the line', () => {
  import assert from 'assert';      
  import {document} from '../test-setup.js';   
});

`;
      const expected = `
// some comment at the beginning of the file

describe('the imports below shall stay, they are not at the beginning of the line', () => {
  import assert from 'assert';      
  import {document} from '../test-setup.js';   
});

`;
      assertThat(removeImports(sourceCode), expected);
    });
  });
});
