#!/bin/node
import path from 'path';

import {DIST_DIR, KATAS_DIR} from './config.js';
import {all as rawMetadataEs1} from '../katas/es1/language/__raw-metadata__.js';
import {all as rawMetadataEs3} from '../katas/es3/language/__raw-metadata__.js';
import {all as rawMetadataEs1LearnByRewriting} from '../katas/es1/learn-by-rewriting/__raw-metadata__.js';
import {all as rawMetadataEs5} from '../katas/es5/language/__raw-metadata__.js';
import {all as rawMetadataEs6} from '../katas/es6/language/__raw-metadata__.js';
import {all as rawMetadataEs7} from '../katas/es7/language/__raw-metadata__.js';
import {all as rawMetadataEs8} from '../katas/es8/language/__raw-metadata__.js';
import {all as rawMetadataEs10} from '../katas/es10/language/__raw-metadata__.js';
import {all as rawMetadataEs11} from '../katas/es11/language/__raw-metadata__.js';
import {all as rawMetadataEs14} from '../katas/es14/language/__raw-metadata__.js';
import {all as rawMetadataHamjest} from '../katas/libraries/hamjest/__raw-metadata__.js';

import {writeToFileAsJson} from './_external-deps/filesystem.js';
import MetaData from './metadata.js';
import FlatMetaData from './flat-metadata.js';
import GroupedMetaData from './grouped-metadata.js';

const destinationDirEs1 = path.join(DIST_DIR, 'katas/es1/language');
const destinationDirEs3 = path.join(DIST_DIR, 'katas/es3/language');
const destinationDirEs1LearnByRewriting = path.join(DIST_DIR, 'katas/es1/learn-by-rewriting');
const destinationDirEs5 = path.join(DIST_DIR, 'katas/es5/language');
const destinationDirEs6 = path.join(DIST_DIR, 'katas/es6/language');
const destinationDirEs7 = path.join(DIST_DIR, 'katas/es7/language');
const destinationDirEs8 = path.join(DIST_DIR, 'katas/es8/language');
const destinationDirEs10 = path.join(DIST_DIR, 'katas/es10/language');
const destinationDirEs11 = path.join(DIST_DIR, 'katas/es11/language');
const destinationDirEs14 = path.join(DIST_DIR, 'katas/es14/language');
const destinationDirHamjest = path.join(DIST_DIR, 'katas/libraries/hamjest');

const buildMetadata = () => {
  new MetaData(writeToFileAsJson)
    .convertWith(rawMetadataEs1, FlatMetaData)
    .writeToFile(path.join(destinationDirEs1, '__all__.json'));
  new MetaData(writeToFileAsJson)
    .convertWith(rawMetadataEs1, GroupedMetaData)
    .writeToFile(path.join(destinationDirEs1, '__grouped__.json'));

  new MetaData(writeToFileAsJson)
    .convertWith(rawMetadataEs3, FlatMetaData)
    .writeToFile(path.join(destinationDirEs3, '__all__.json'));
  new MetaData(writeToFileAsJson)
    .convertWith(rawMetadataEs3, GroupedMetaData)
    .writeToFile(path.join(destinationDirEs3, '__grouped__.json'));

  new MetaData(writeToFileAsJson)
    .convertWith(rawMetadataEs1LearnByRewriting, FlatMetaData)
    .writeToFile(path.join(destinationDirEs1LearnByRewriting, '__all__.json'));
  new MetaData(writeToFileAsJson)
    .convertWith(rawMetadataEs1LearnByRewriting, GroupedMetaData)
    .writeToFile(path.join(destinationDirEs1LearnByRewriting, '__grouped__.json'));

  new MetaData(writeToFileAsJson)
    .convertWith(rawMetadataEs5, FlatMetaData)
    .writeToFile(path.join(destinationDirEs5, '__all__.json'));
  new MetaData(writeToFileAsJson)
    .convertWith(rawMetadataEs5, GroupedMetaData)
    .writeToFile(path.join(destinationDirEs5, '__grouped__.json'));

  new MetaData(writeToFileAsJson)
    .convertWith(rawMetadataEs6, FlatMetaData)
    .writeToFile(path.join(destinationDirEs6, '__all__.json'));
  new MetaData(writeToFileAsJson)
    .convertWith(rawMetadataEs6, GroupedMetaData)
    .writeToFile(path.join(destinationDirEs6, '__grouped__.json'));

  new MetaData(writeToFileAsJson)
    .convertWith(rawMetadataEs7, FlatMetaData)
    .writeToFile(path.join(destinationDirEs7, '__all__.json'));
  new MetaData(writeToFileAsJson)
    .convertWith(rawMetadataEs7, GroupedMetaData)
    .writeToFile(path.join(destinationDirEs7, '__grouped__.json'));

  new MetaData(writeToFileAsJson)
    .convertWith(rawMetadataEs8, FlatMetaData)
    .writeToFile(path.join(destinationDirEs8, '__all__.json'));
  new MetaData(writeToFileAsJson)
    .convertWith(rawMetadataEs8, GroupedMetaData)
    .writeToFile(path.join(destinationDirEs8, '__grouped__.json'));

  new MetaData(writeToFileAsJson)
    .convertWith(rawMetadataEs10, FlatMetaData)
    .writeToFile(path.join(destinationDirEs10, '__all__.json'));
  new MetaData(writeToFileAsJson)
    .convertWith(rawMetadataEs10, GroupedMetaData)
    .writeToFile(path.join(destinationDirEs10, '__grouped__.json'));

  new MetaData(writeToFileAsJson)
    .convertWith(rawMetadataEs11, FlatMetaData)
    .writeToFile(path.join(destinationDirEs11, '__all__.json'));
  new MetaData(writeToFileAsJson)
    .convertWith(rawMetadataEs11, GroupedMetaData)
    .writeToFile(path.join(destinationDirEs11, '__grouped__.json'));

  new MetaData(writeToFileAsJson)
    .convertWith(rawMetadataEs14, FlatMetaData)
    .writeToFile(path.join(destinationDirEs14, '__all__.json'));
  new MetaData(writeToFileAsJson)
    .convertWith(rawMetadataEs14, GroupedMetaData)
    .writeToFile(path.join(destinationDirEs14, '__grouped__.json'));

  new MetaData(writeToFileAsJson)
    .convertWith(rawMetadataHamjest, FlatMetaData)
    .writeToFile(path.join(destinationDirHamjest, '__all__.json'));
  new MetaData(writeToFileAsJson)
    .convertWith(rawMetadataHamjest, GroupedMetaData)
    .writeToFile(path.join(destinationDirHamjest, '__grouped__.json'));
};

import {convertTestsToKatas} from './convert-tests-to-katas/index.js';

convertTestsToKatas({sourceDir: KATAS_DIR, destinationDir: path.join(DIST_DIR, 'katas')})
  .then(buildMetadata)
;
