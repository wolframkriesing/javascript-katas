import os from "os";
import {inspectFile} from 'tests-inspector';
import {runTest} from "./runner.js";

const collectSubTests = (testSuite) => {
  const {tests, suites} = testSuite;
  const childTests = suites.reduce((tests, suite) => {
    const childTests = collectSubTests(suite);
    return tests.concat(childTests);
  }, []);
  return tests.concat(childTests);
};

async function findAllTests(filename) {
  const rootTestSuite = await inspectFile(filename);
  return collectSubTests(rootTestSuite);
}

const stats = {fails: 0, passes: 0, runs: 0};

const readRunnerOptions = (sourceCode) => {
  const firstLine = sourceCode.trim().split('\n')[0].trim();
  if (firstLine.startsWith('//: {"jskatas"')) {
    const optionsString = firstLine.substring(3).trim();
    return JSON.parse(optionsString)?.jskatas.runnerOptions;
  }
  return {};
};

const runOneTest = async (test, filename) => {
  const runnerOptions = readRunnerOptions(test.sourceCode);
  const ran = await runTest(test.sourceCode, runnerOptions);
  if (ran.success) {
    stats.passes++;
  } else {
    console.log('');
    console.log(`❌  ${test.name} (${filename}:${test.startLine}})`);
    console.log(`     ${ran.message}`);
    console.log(`\`\`\`  ${test.sourceCode}\n\`\`\``);
    stats.fails++;
  }
};

const numCpus = os.cpus().length;
let pool = [];

const waitForPoolToFinish = async () => {
  await Promise.all(pool);
  pool = [];
};

const addTestToPool = async (test, filename) => {
  pool.push(runOneTest(test, filename));
  if (pool.length < numCpus) {
    return;
  }
  await waitForPoolToFinish();
};

const runEachTest = async (filename) => {
  const allTests = await findAllTests(filename);
  for (const test of allTests) {
    const kata = filename.replace('katas/', '').replace('language/', '').replace('.js', '').padEnd(50, ' '); // pad at the end to have no leftovers, I didnt get them removed any other way
    process.stdout.write(`\rtest # ${stats.runs} - ${kata}`);
    await addTestToPool(test, filename);
    stats.runs++;
  }
  await waitForPoolToFinish();
}

console.time('Overall');
console.log(`Running autonomous tests, ensuring they run independently (in workers on ${numCpus} CPUs/cores) ...`);
const kataFilenames = process.argv.slice(2);
for await (const filename of kataFilenames) {
  await runEachTest(filename);
}

const padTestCount = (num) => String(num).padStart(String(stats.passes).length, ' ');
console.log('\n----------------------------------------');
console.log(`Ran ${stats.runs} tests`);
console.log(`✅  ${padTestCount(stats.passes)} tests passed (${Number(stats.passes / stats.runs * 100).toFixed(1)}%)`);
const failIcon = stats.fails > 0 ? '❌' : '😎';
console.log(`${failIcon} ${padTestCount(stats.fails)} tests failed (${Number(stats.fails / stats.runs * 100).toFixed(1)}%)`);

console.timeEnd('Overall');
