import fs from "fs";
import path from "path";
import {Worker} from "node:worker_threads";
import {fileURLToPath} from "url";

/**
 * The following are the types used in this file.
 * 
 * @typedef {string} SourceCode
 * @typedef {{strictMode: boolean} | {topLevelAwait: boolean} | {runGivenCodeOnly: boolean, type?: 'module'}} RunnerOptions
 * @typedef {{success: boolean, message: string, code?: unknown}} TestRunResult
 */

const __filename = fileURLToPath(import.meta.url);
const projectRoot = path.resolve(__filename, '../../..');
const projectName = JSON.parse(fs.readFileSync(path.resolve(projectRoot, 'package.json'), 'utf-8')).name;
if (projectName !== 'javascript-katas') {
  throw new Error(`Either the package.json is not found or the package name is not 'jskatas', but '${projectName}'`);
}
const tmpDir = path.join(projectRoot, 'tmp');
fs.mkdirSync(tmpDir, {recursive: true});
const testSetupFile = path.join(projectRoot, 'katas/test-setup.cjs');

/** @type {RunnerOptions} */
const defaultOptions = {strictMode: true, topLevelAwait: false, runGivenCodeOnly: false};

/**
 * This is the main entry point for the test runner.
 * 
 * @param sourceCode {SourceCode}
 * @param options {RunnerOptions?}
 * @returns {Promise<TestRunResult>}
 */
export const runTest = async (sourceCode, options) => {
  const mergedOptions = {...defaultOptions, ...options};
  return await runCodeInWorker(sourceCode, mergedOptions);
}

/**
 * This runs the given source code in a worker thread, configuring it as given by the options.
 * 
 * @param sourceCode {SourceCode}
 * @param options {RunnerOptions}
 * @returns {Promise<TestRunResult>}
 */
const runCodeInWorker = async (sourceCode, options) => {
  let theResolve;
  const returnPromise = new Promise((resolve) => {
    theResolve = resolve;
  });

  const asModule = options.topLevelAwait;
  const lines = [
    options.strictMode ? `'use strict';` : '',
    asModule 
      ? [
        'import assert from "assert";',
        `import {document} from "${testSetupFile}";`
      ].join('\n')
      : [
        'const assert = require("assert");',
        `const {document} = require("${testSetupFile}");`
      ].join('\n'),
  ];
  const blob = options.runGivenCodeOnly ? sourceCode : 
    lines.join('\n') + `
    ${sourceCode}
  `;

  const uniqueId = Math.random().toString(36).substring(2);
  // Write blob to a temporary JS file
  const extension = (asModule || options?.type === 'module') ? 'mjs' : 'cjs';
  const tmpFile = path.join(tmpDir, `jskata-test-${uniqueId}.${extension}`);
  fs.writeFileSync(tmpFile, blob);

  const closeWorker = (worker) => {
    worker.terminate();
    fs.existsSync(tmpFile) && fs.unlinkSync(tmpFile);
  };

  // Trying to run the worker thread without creating a file fails ... the imports are tricky to "see" the node-modules and the `test-setup.js` file, so we create a temporary file for now.
  // This is quite slow, but works well.
  // const worker = new Worker(new URL('data:text/javascript,'+blob));
  // const worker = new Worker(blob, { eval: true });
  // const worker = new Worker(new URL('data:text/javascript,'+blob));
  const worker = new Worker(new URL('file:///' + tmpFile));

  worker.on('error', (event) => {
    theResolve({success: false, message: event.message ?? String(event)});
    closeWorker(worker);
  });
  worker.on('exit', (code) => {
    if (code === UNFINISHED_TOP_LEVEL_AWAIT) {
      theResolve({success: false, message: 'Promise is pending', testRunnerId: uniqueId});
      // Maybe in the future we dont want to terminate the worker and not remove the file yet ... 🤔 ... so we can get back to see if the worker finished ...
    } else {
      theResolve({success: true, message: 'Exited', code});
    }
    closeWorker(worker);
  });
  return returnPromise;
}

const UNFINISHED_TOP_LEVEL_AWAIT = 13;
