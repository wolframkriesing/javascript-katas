import {describe, it} from 'mocha';
import {anything, assertThat, hasProperties} from "hamjest";
import {runTest} from "./runner.js";

describe('The test runner', () => {
  describe('runs each test in isolation, without any side-effect across tests', () => {
    it('WHEN test1 defines `x` in global scope THEN tests must NOT see `x`', async () => {
      await runTest(`x = 42;`, 0);
      assertThat(await runTest('assert.equal(typeof x, "undefined")'), hasProperties({success: true, code: 0}));
    });
  });
  describe('Allows to configure strict mode', () => {
    it('WHEN the mode is not given THEN it defaults to strict mode', async () => {
      const sourceCode = `
        const isStrict = function() { return !this; }; // A function to test for strict mode, since "this" is undefined in strict mode.
        if (false === isStrict()) 
          throw(Error('non-strict'));
      `;
      assertThat(await runTest(sourceCode), hasProperties({success: true}));
    });
    it('WHEN strict-mode wants to be turned off THEN this must be given explicitly', async () => {
      const sourceCode = `
        const isStrict = (function() { return !this; })(); // A function to test for strict mode, since "this" is undefined in strict mode.
        if (false === isStrict) 
          throw(Error('non-strict'));
      `;
      assertThat(await runTest(sourceCode, {strictMode: false}), hasProperties({"success": false, "message": "non-strict"}));
    });
  });
  describe('Allow to run just the code passed to the test runner', () => {
    it('WHEN the option runOnlyCodeAsGiven=true THEN there is no code added, the code is run as given', async () => {
      // The code below fails because it turns on strict mode and then makes the code run in the test-runner fail
      // by assigning a variable that is not declared, which throws in strict mode.
      // Of course, this assumes the code is run purely in non-strict mode and is turned on by the "use strict" given below.
      // But that is acceptable. I don't know a more pragmatic way for now.
      assertThat(await runTest("'use strict'; x = 1", {runGivenCodeOnly: true}), hasProperties({"success": false, message: 'x is not defined'}));
    });  
  });
  describe('Promises, or slow tests', () => {
    it('WHEN a test is slow or delayed, e.g. using a timeout THEN the runner waits for the result', async () => {
      const sourceCode = `
        await new Promise((resolve) => setTimeout(() => resolve('done after timeout'), 10));
      `;
      assertThat(await runTest(sourceCode, {topLevelAwait: true}), hasProperties({"success": true}));
    });
    it('WHEN a promise rejects THEN the result is returned', async () => {
      const sourceCode = `
        await Promise.reject('failed right away');
      `;
      assertThat(await runTest(sourceCode, {topLevelAwait: true}), {"success": false, "message": "failed right away"});
    });
    it('WHEN a promise rejects with a delay THEN the result is returned', async () => {
      const sourceCode = `
        await new Promise((_, reject) => setTimeout(() => reject('failed after timeout'), 10));
      `;
      assertThat(await runTest(sourceCode, {topLevelAwait: true}), {"success": false, "message": "failed after timeout"});
    });
    it('WHEN a promise is pending THEN this is reported back', async () => {
      const sourceCode = `
        await new Promise(() => {});
      `;
      assertThat(await runTest(sourceCode, {topLevelAwait: true}), hasProperties({
        "success": false,
        "message": "Promise is pending",
        testRunnerId: anything(), // This might be needed to continue the thread and eventually finish it.
      }));
    });
  });
});
